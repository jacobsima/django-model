from django.contrib import admin
from .models import Post,Person, Course,Manufacturer,Topping, Pizza,AddFavorite,Musician,Group,Membership
# Register your models here.
admin.site.register(Post)
admin.site.register(Person)
admin.site.register(Course)
admin.site.register(Manufacturer)
admin.site.register(Topping)
admin.site.register(Pizza)
admin.site.register(AddFavorite)
admin.site.register(Membership)
admin.site.register(Musician)
admin.site.register(Group)

