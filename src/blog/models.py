from django.db import models
from django.conf import settings
# from django.contrib.auth.models import User
import uuid

User = settings.AUTH_USER_MODEL

# Model describe different ways to declare the id of a model
#===========================================================
# print(dir(models))   : to get all properties and ,methods of models
class Person(models.Model):
  # AutoField: An IntegerField that automatically increments according to available IDs
  # id = models.AutoField(primary_key=True)   # Set automatically 
  first_name = models.CharField(max_length=50)
  last_name = models.CharField(max_length=50)
  age = models.PositiveIntegerField(blank=True, null=True)
  

  # person = Person.objects.get(pk=1)
  # person.fields_set.all() : to get the reverse row create in DB
  # person.fields_set.count()

  def __str__(self):
      return self.first_name
  

  #   CREATE TABLE person_post (
  #     "id" serial NOT NULL PRIMARY KEY,
  #     "first_name" varchar(30) NOT NULL,
  #     "last_name" varchar(30) NOT NULL
  # );



# A 64-bit integer, much like an AutoField except that it is guaranteed to fit numbers from 1 to 9223372036854775807.
class Post(models.Model): # This will be the list(Many-to-Many)
  id = models.BigAutoField(primary_key=True)
  title = models.CharField(max_length=50)

  # user = User.objects.get(username='testuser')
  # user.auths.all()  : to query this from user, as it create a reverse field at user table,return a QuerySet[]
  # user.auths.create(title='made from shell') : to create a post from reverse user 
  # User.objects.filter(auths__title='Courir comme jamais') = <QuerySet [<User: admin>]>

  author = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={'is_staff': True}, related_name='auths') # from default value, we set the id of 1 as admin , you set the id of the user as the default value

  # limit_choices_to : only user with staff privelege can be able to have access on this.


  def __str__(self):
      return self.title
  
class AddFavorite(models.Model): # this will be created by choosing one-many-Post (Many-to-Many)
  author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
  posts  = models.ManyToManyField(Post)

  # Create an addfavorite and add post on in
  # u1 = User.objects.get(id=1)
  # a1 = AddFavorite(author=u1)
  # a1.save()
  # p2 = Post(title='lelan', author=u1)
  # p2.save()
  # a1.posts.add(p2)
  # a1.posts.all()
  # p2.addfavorite_set.all()






 




  


# Use uuid to create an id on the model
class Course(models.Model):
  id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
  subject = models.CharField(max_length=200)

  # ForeignKey used at StudentAbstractInfo, which is used  at StudentWithCar
  # course = Course.objects.get(subject='history')
  # course.studentwithcar_set.all()
  # course.studentwithcar_set.count()

  def __str__(self):
      return self.subject
  

# Added during ForeignField and Abstract Topic
class Manufacturer(models.Model):
  name = models.CharField(max_length=200)

  # ForeignKey used at StudentAbstractInfo, which is used  at StudentWithCar
  # manufacturer = Manufacturer.objects.get(name='toyota')
  # manufacturer.studentwithcar_set.all()
  # manufacturer.studentwithcar_set.count()

  def __str__(self):
      return self.name
  


# Many-to-Many 
# This will create a special table pizza_toppings by default to be the combination of both tables
# in total, 3 tables will be create, toppings,pizzas and pizaa_toppings
# pizza_toppings will have, its own id, pizza_id, topping_id
class Topping(models.Model):  # This will be the list(Many-to-Many)
  top_name = models.CharField(max_length=20,null = True, blank=True)

  def __str__(self):
      return self.top_name
  

class Pizza(models.Model):   # this will be created by choosing one-many-Topping (Many-to-Many)
  pizza_name = models.CharField(max_length=30)
  toppings = models.ManyToManyField(Topping)

  def __str__(self):
      return self.pizza_name





# Many-To-Many via Through
class Musician(models.Model):  # list of musicians
  name = models.CharField(max_length=128)

  def __str__(self):
      return self.name


class Group(models.Model):  # list of different group
  name = models.CharField(max_length=128)
  members = models.ManyToManyField(Musician,through='Membership')
  
  def __str__(self):
      return self.name
  
  

class Membership(models.Model): # create many-to-many relation here
  person = models.ForeignKey(Musician, on_delete=models.CASCADE)
  group = models.ForeignKey(Group,on_delete=models.CASCADE)
  date_joined = models.DateField()
  invite_reason = models.CharField(max_length=64)




 


