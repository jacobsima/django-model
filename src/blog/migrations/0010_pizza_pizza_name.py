# Generated by Django 3.0.8 on 2020-07-11 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_pizza_topping'),
    ]

    operations = [
        migrations.AddField(
            model_name='pizza',
            name='pizza_name',
            field=models.CharField(default='veg', max_length=30),
            preserve_default=False,
        ),
    ]
