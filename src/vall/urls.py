from django.urls import path
from .views import val_home


urlpatterns = [
    path('', val_home, name='val-app')
]
