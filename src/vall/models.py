from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save,pre_delete,post_delete
from django.dispatch import receiver


def validate_cutomer_name(value):
  if '#' in value or '@' in value:
    raise ValidationError("('@','#'),These character are not allowed in title")
  else:
    return value

def validate_email(value):
  if not '@' in value :
    raise ValidationError('@ sign missing on email') 
  else:
    return value 

class Cutomer(models.Model):
  name               = models.CharField(max_length=200, validators=[validate_cutomer_name])
  active             = models.BooleanField(default=False)
  joined_at          = models.DateTimeField(auto_now_add=True)
  last_register      = models.DateTimeField(default=timezone.now)
  promotor           = models.ForeignKey(User, on_delete=models.CASCADE)
  phone              = models.IntegerField(null=True, blank=True)
  email              = models.CharField(max_length=200, null=True,blank=True, validators=[validate_email])
  slug               = models.SlugField(null=True, blank=True)

  # by default django models has this objects objects = Manager()
  

  # custom method : 
  # c1 = Cutomer.objects.all().first()
  # c1.name_phone()
  # use decorator to convert this method to property
  # now with this property decorator you can access with : c1.name_phone
  @property
  def name_phone(self):
    return f'{self.name}: {self.phone}'

  
  
  # Override Save Method
  def save(self,*args, **kwargs): 
    # self.name = self.name + '-' +  str(self.promotor)
    self.slug = slugify(self.name)
    super().save(self, *args, **kwargs)
    
    

  def __str__(self):
      return self.name

  class Meta:

    # abstract = True # this model will be an abstract base class.

    # To order by name descending, then by promotor ascending, use this:
    # ordering = ['-name', 'promotor']

    # phone ascending, use this:
    ordering = ['phone'] 

    # The plural name for the object:
    verbose_name_plural = 'Cutomers'

    # A human-readable name for the object, singular:
    verbose_name = "customer"

    # The name of the database table to use for the model:
    db_table = 'customers'
    
    
    # If False, no database table creation, modification, or deletion operations will be performed for this model. This is useful if the model represents an existing table or a database view that has been created by some other means.
    # managed = False # connect to DB but do not do any migrations,Default = True

   
    #  A list of indexes that you want to define on the model:
    # indexes = [
    #         models.Index(fields=['name', 'promotor']),
    #         models.Index(fields=['promotor'], name='promotor_idx'),
    #     ]
  


# Signal part
class SignalClass(models.Model):
  s_name = models.CharField(max_length=200)
  s_last_name= models.CharField(max_length=200)
  slug = models.SlugField(null=True, blank=True)

  def __str__(self):
      return self.s_name




@receiver(post_save,sender=SignalClass)   #sender:  The model class.
def post_sign(sender,instance,created,**kwargs):
  print('Post-save')
  if created:  # A boolean; True if a new record was created.
    print('created - Signall')
    # The actual instance(object) being saved.
    instance.slug  = slugify(f'post save {instance.s_name}')
    instance.save()



@receiver(pre_save,sender=SignalClass)  
def pre_sign(sender,instance,**kwargs):
  print('Pre-save')



@receiver(pre_delete,sender=SignalClass)  
def pre_del__sign(sender,instance,**kwargs):
  print('Pre-delete')
  print(f'{instance.s_name} will be deleted')


@receiver(post_delete,sender=SignalClass)  
def post_del__sign(sender,instance,**kwargs):
  print('Post-delete')
  print(f'{instance.s_name} has been deleted')