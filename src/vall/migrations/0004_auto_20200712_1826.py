# Generated by Django 3.0.8 on 2020-07-12 16:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vall', '0003_auto_20200712_1823'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cutomer',
            options={'ordering': ['phone'], 'verbose_name': 'pizza', 'verbose_name_plural': 'Cutomers'},
        ),
    ]
