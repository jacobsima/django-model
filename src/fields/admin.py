from django.contrib import admin
from .models import Fields,Choice,Student,StudentWithCar

admin.site.register(Fields)
admin.site.register(Choice)
admin.site.register(Student)
admin.site.register(StudentWithCar)

