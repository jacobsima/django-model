from django.db import models
from django.utils import timezone


#************************  Date , null, blank *****************************

# DateTimeField =  datetime.datetime in python 
# DateTimeField: default=timezone.now - from django.utils.timezone.now()


# DateField     =  datetime.date  in python
# DateField: default=date.today - from datetime.date.today()

# DateTimeField.auto_now_add
"""
# auto_now_add=True => set system current date and time when this object is created,you cannot change the value of the date posted,editable is False

# auto_now_add=False => The date can be change or set, editable is True
"""

# DateTimeField.auto_now
"""
 
Automatically set the field to now every time the object is saved. Useful for “last-modified” timestamps.
The field is only automatically updated when calling Model.save(). The field isn’t updated when making updates to other fields in other ways such as QuerySet.update()
 
 auto_now => True,this field will be added to all column on the table with the current date as last updated, then each time to change an item and save it, it capture the time as last_updated. Editable=False. you can not manually set this time, set from the system time automatically.

 auto_now => False,Editable true, won't change everytime we make an update with save()

"""

# Datetime 
class Fields(models.Model):
  title       = models.CharField(max_length=100)
  active      = models.BooleanField(default=True)

  create_date  = models.DateTimeField(auto_now_add=True)

  # need a default value since set to False
  # run an empty migration to insert this column
  # ran empty migrations first then then wrote migrations code to add this field
  # and default value => 0002_auto_20200706_1044.py
  created_date_2 = models.DateTimeField(auto_now_add=False)

  # new migrations for this
  last_upadted   = models.DateTimeField(auto_now=True)

  # need a default value since set to False
  # run an empty migration to insert this column
  # 0004_auto_20200708_1937.py
  last_upadted_2   = models.DateTimeField(auto_now=False)

  # just ran normal migrations and add the field even for all existing ones to current timezone, editable=True
  date_posted = models.DateTimeField(default=timezone.now)


  # null  = True  => the field will be set to Null in the DB, default is False

  # blank = True  => determine if the field will be required from the form, if True then this field is not required the form can submit even if it is omitted, default is False
  desciption = models.CharField(max_length=200, null=True, blank=True)

  # person = Person.objects.get(pk=1)
  # person.fields_set.all() : to get the reverse row create in DB
  person = models.ForeignKey('blog.Person', on_delete=models.CASCADE)






#************************  Choice *****************************

# Choice :  the default form widget will be a select box
# The first element in each tuple is the actual value to be set on the model, and the second element is the human-readable name

class Choice(models.Model):

  CATEGORY_CHOICE = (
    ('D', 'Django'),
    ('N','Node JS')
  )
  category  = models.CharField(max_length=1, choices=CATEGORY_CHOICE,default='D')

  # verbose name as first argument, just a meaning name Show 
  title     = models.CharField("choice title",max_length=100,null=True, blank=False, help_text='This is just a title')
  available = models.BooleanField(default=False)

# to create from shell
"""
choice = Choice(title='shell Title',availabe=True, category='D')
choice.save()
p.get_category_display() = Django  =. to access the category value in views or form
"""






#************************  ForiegnKey- Abstract  *****************************
# ForiegnKey: A many-to-one relationship
from utils.abstract   import CommonInfo,StudentAbstractInfo

# ****  Abstract 
# The Student model will have four fields: name, age, graduation_year and course. The CommonInfo model cannot be used as a normal Django model, since it is an abstract base class. It does not generate a database table or have a manager, and cannot be instantiated or saved directly.
# Fields inherited from abstract base classes can be overridden with another field or value, or be removed with None.
class Student(CommonInfo):
  graduation_year = models.CharField(max_length=200)
  course = models.CharField(max_length=100)




class StudentWithCar(CommonInfo,StudentAbstractInfo):
  last_name = models.CharField(max_length=200)
  student_number = models.IntegerField()
  car_model   = models.CharField(max_length=200)

  # name and age : from CommonInfo abstract,
  # manufacturer and course : from StudentAbstractInfo

  # course = Course.objects.get(subject='history')
  # course.studentwithcar_set.all()
  # course.studentwithcar_set.count()

  # ForeignKey used at StudentAbstractInfo, which is used  at StudentWithCar
  # manufacturer = Manufacturer.objects.get(name='toyota')
  # manufacturer.studentwithcar_set.all()
  # manufacturer.studentwithcar_set.count()







