# Abstract base classes
from django.db import models

class CommonInfo(models.Model):
  name = models.CharField(max_length=200)
  age  = models.IntegerField()

  class Meta:
    abstract = True




class StudentAbstractInfo(models.Model):

  # if manufacturer object deleted, then do not delete the model object that uses this
  # the field can be null on the DB
  manufacturer = models.ForeignKey('blog.Manufacturer',on_delete=models.SET_NULL,null=True,blank=True)

  # if course object deleted, then cascade the delete, delete any other object that uses this as foreignKey
  course = models.ForeignKey('blog.Course',on_delete=models.CASCADE)

  class Meta:
    abstract = True

