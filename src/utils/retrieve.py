# B : Retrieving  objects:
# To retrieve objects from your database, construct a QuerySet via a Manager on your model class.
# if defaault Manager used on Model Post then : (Post.objects) will be used to construct a QuerySet.
#  It can have zero, one or many filters.


# B 1. Methods that return new QuerySets(an list of objects)

   # B 1. 1 Person Model : Normal Fields      
            # persons = Person.objects.all()



   # B 1. 2 Choice Model: Normal Fields  with choices
            # choices = Choice.objects.all()
   

   # B 1. 3 Post Model: Field with ForeignKey on author, using User model
            # posts = Post.objects.all()
        
            # Get all post from a specific User
            # user_1 = User.objects.get(pk=1)
            # post_from_user_1 = user_1.auths.all()


  # B 1. 4 Fields Model: ForeignKey Fields with No related_name used  
            # fields = Fields.objects.all() 

            # Get all Field from a specific Person  
            # person_1 = Person.objects.get(pk=1)
            # person_1.fields_set.all()
  
  # B 1. 5 StudentWithCar: With Two ForeginKey Fields
           # std_1 = StudentWithCar.objects.all()

           # Get all StudentWithCar from a specific Course
           # course = Course.objects.get(subject='History')
           # course.studentwithcar_set.all()

           # Get all StudentWithCar from a specific Manifacturer
           # man = Manufacturer.objects.get(name='toyota')
           # man.studentwithcar_set.all()



  # B 1. 6 Topping and Pizza : Model ManyToManyField Relation  without (through model)

            # tops = Topping.objects.all()
            # pizzas = Pizza.objects.all()

            # Get all pizza from a specific Topping
            # top_1 = Topping.objects.get(pk=1)
            # top_1.pizza_set.all()
            # top_1.pizza_set.count()

  # B 1. 7 Musician and Group through Membership : Model ManyToManyField Relation  with through model

            # groups = Group.objects.all()
            # musicians = Musician.objects.all()
            # members = Membership.objects.all()

            # gr_1 = Group.objects.get(pk=1)
            # mus_1 = Musician.objects.get(pk=1)
            # member = Membership.objects.get(pk=1)

            # gr_1.membership_set.all()  : get all object that have used this group
            # mus_1.membership_set.all() : get musician all memebrship joined
    




























                           



 


