# This file isn't ran in this application. Only saved as the queries tutorial list to go through in the shell.

# A : Creating and Saving changes to an objects:
# create: return the new object create 
# save  : Does not return anything
# To Create and save an object in a single step, use the create() method.

# A 1. Normal Field
# A 1. 1 Person Model (blog-models-Person)

        #****** This performs an INSERT SQL statement behind the scenes(Create)
        #  Person.objects.create(first_name='Deo',last_name='kanda',age=31);
        # d = Person(first_name='Lebo',last_name='teka',age=43);
        # d.save();
        
        #***** This performs an UPDATE SQL statement behind the scenes(Saving Changes)
        # d.first_name ='Tuta';
        # d.save() : Django doesn’t hit the database until you explicitly call save().

# A 1. 1 Choice Model (fields-models-Choice) 

        #****** This performs an INSERT SQL statement behind the scenes(Create)
        # choice_1 = Choice.objects.create(category='N',title='Rolling Nodejs',available=True)
        # choice_2 = Choice(category='D',title='Rolling Django', available=False)
        # choice_2.save()

        #***** This performs an UPDATE SQL statement behind the scenes(Saving Changes)
        # choice_1.category = 'D'
        # choice_1.title = 'Rolling Django from Nodejs'
        # choice_1.save()

# A 2. Normal and One ForeignKey Fields (related_name='auths')
# A 2. 1 Post Model (blog-models-Post) with User Model as ForeignKey used for author
      
        #****** This performs an INSERT SQL statement behind the scenes(Create)
        # user1 = User.objects.get(pk=1) => must return an object.
        # post1 = Post.objects.create(title='lemba nga un',author=user1) 
        # post2 = Post(title='matete deux',author=user1)
        # post2.save()

        #****** Reverse Creation from User model using(related_name='auths')
        # user1 = User.objects.get(pk=1)
        # post = user1.auths.create(title='reverse creation from user model')

        #***** This performs an UPDATE SQL statement behind the scenes(Saving Changes)
        # user2 =  User.objects.get(username='testuser')
        # post1.author = user2
        # post1.save()

# A 3. Normal and One ForeignKey Fields with No related_name used
# A 3. 1 Fields Model (fields-models-Fields) with Person model as a ForienKey used person

        #****** This performs an INSERT SQL statement behind the scenes(Create) 
        # person_1 = Person.objects.get(first_name='Lena')
        # field_1 = Fields.objects.create(
        #               title='F-One',
        #               active=False,
        #               last_upadted_2='2020-07-13',
        #               created_date_2='2020-07-13',
        #               desciption='meka-one',
        #               person=person_1)

        # field_2 = Fields(
        #               title='F-Two',
        #               active=False,
        #               last_upadted_2='2020-07-13',
        #               created_date_2='2020-07-13',
        #               desciption='meka-Two',
        #               person=person_1)
        # field_2.save()

        #****** Reverse Creation from User model
        # person_2 = Person.objects.get(first_name='Phila')
        # field_2 = person_2.fields_set.create(
        #               title='F-Reverse creation from person 3',
        #               active=True,
        #               last_upadted_2='2020-07-13',
        #               created_date_2='2020-07-13',
        #               desciption='We do reverse from Person Model using "person_2.fields_set.create"')
        #                          


# A 4. Normal and Two or more ForeignKey Fields
# A 4 .1 StudentWithCar Model (fields-models-StudentWithCar)

        # Simple Create
        # man_1 = Manufacturer.objects.get(pk=1)
        # course_1 = Course.objects.get(subject='History')
        # stc_1  = StudentWithCar.objects.create(
        #            name = 'Benzi',
        #            age = 32 ,
        #            course = course_1,
        #            manufacturer = man_1,
        #            last_name = 'kundeluka',
        #            student_number = 32174324,
        #            car_model = 'Honda-benxa' )    

        # Reverse Creation from manufacturer 
        # stc_2 = man_1.studentwithcar_set.create(
        #            name = 'Reverse from manufacturer',
        #            age = 22 ,
        #            course = course_1,
        #            last_name = 'Mbetu',
        #            student_number = 219373,
        #            car_model = 'Honda-Zungu')

        # Reverse Creation from course
        # stc_3 =    course_1.studentwithcar_set.create(
        #            name = 'Reverse from course',
        #            age = 14 ,
        #            manufacturer = man_1,
        #            last_name = 'Mbala',
        #            student_number = 9383723,
        #            car_model = 'Honda-Bulunbu') 
        
# A 5. Model ManyToManyField Relation  without (through model) 
# A 5. 1 Topping and Pizza 
       
        # Create
        # top_1 = Topping.objects.create(top_name='first top')      
        # top_2 = Topping.objects.create(top_name='second top')      
        # top_3 = Topping.objects.create(top_name='third top') 
        # pizza_1 = Pizza.objects.create(pizza_name='Soupu na tolo') 
        # pizza_1.toppings.add(top_1) 
        # pizza_1.toppings.add(top_2, top_3)

        # Reverse Create 
        # top_4 = Topping.objects.get(top_name='Red Chilli')
        # top_4.pizza_set.create(pizza_name='mayesa')

# A 6. Model ManyToManyField Relation  with (through model) 
# A 6. 1 Musician and Group Through Membership 

        # Create
        # mus_1 = Musician.objects.create(name='Chikito')
        # gr_1  = Group.objects.create(name='vivala musica')
        # member_1 = Membership.objects.create(person=mus_1, group=gr_1,date_joined='2010-07-12',invite_reason='bilobela')
        
        # Reverse Creation = No Possible
        
                           



 


