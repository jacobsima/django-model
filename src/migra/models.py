# In this Application Will going to deal Only Migrations
# https://docs.djangoproject.com/en/3.0/ref/migration-operations/
# python manage.py makemigrations --empty blog : to create a empty migration
import uuid
from django.db import models

# Create your models here.
class Car(models.Model):
  id = models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
  name = models.CharField(max_length=100)
  model = models.CharField(max_length=120)
  man_year = models.PositiveIntegerField(blank=True, null=True)
  

# NB : 
# 1. Run makemigrations migra to create migration files, and alter field as many times you can before running migrate. This operation is safe since You have not yet migrate to the DB 

# After migration: now let us add a new field detail non-nullable field check message 1
  detail = models.CharField(max_length=200)

# After this , let add a nullable field, migrate to DB without any problem
  content = models.CharField(max_length=200, null=True, blank=True)

# Create a new fullname field from makemigrations migra 
# migration done, the save method overwrite down will save the fullname of the lastest car
# to be able to do this for all car we run migrations operation in,
# python manage.py makemigrations migra --empty, file create.See:0006_auto_20200704_0817.py 
# Write your django code on the file and migrate to DB
  fullname = models.CharField(max_length=200,blank=True)


  
  def save(self,*args, **kwargs):
    if not self.fullname:
      self.fullname = f'{self.name}-{self.model}-{self.man_year}'
    super(Car,self).save(*args, **kwargs)


# Message 1 :
"""
You are trying to add a non-nullable field 'detail' to car without a default; we can't do that (the database needs
something to populate existing rows).
Please select a fix:
 1) Provide a one-off default now (will be set on all existing rows with a null value for this column)
 2) Quit, and let me add a default in models.py
Select an option: 1
Please enter the default value now, as valid Python
The datetime and django.utils.timezone modules are available, so you can do e.g. timezone.now
Type 'exit' to exit this prompt
# >>> sell by us
Invalid input: invalid syntax (<string>, line 1)
# >>> 'sell by us'
Migrations for 'migra':
  migra\migrations\0003_car_detail.py
    - Add field detail to car
"""


  
